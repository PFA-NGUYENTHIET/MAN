let rec fact n =
  match n with
  | 0 -> 1
  | _ -> fact (n-1)
  ;;

let rec fibonacci n =
  match n with
  | 0 -> 0
  | 1 -> 1
  | _ -> fibonacci (n - 1) + fibonacci (n-2)
  ;;

let rec pair n = (n = 0) || impair(n-1) and
let rec impair n = (n <> 0) && pair(n-1) ;;